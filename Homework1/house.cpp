// All work performed by Michael DeMaria
// Section 4

#include "house.h"
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

House::House(int s, char b, char f)
{
	if (s < 3)
		s = 3;
	else if (s > 37)
		s = 37;
	
	size = s;

	if (SetBorder(b) == false)
		SetBorder('X');
	else
	{
		border = b;
	}

	if (SetFill(f) == false)
		SetFill('*');
	else
	{
		fill = f;
	}

}

void House::Grow()
{
	if ((size + 1) > 37)
		return;
	
	size++;
}

void House::Shrink()
{
	if ((size - 1) < 3)
		return;
	
	size--;
}

bool House::SetBorder(char b)
{
	if (int(b) < 33 || int(b) > 126)
	{
		border = 'X';
		return false;
	}
	else
	{
		border = b;
		return true;
	}
}

bool House::SetFill(char f)
{
	if (int(f) < 33 || int(f) > 126)
	{	
		fill = '*';
		return false;
	}
	else
	{
		fill = f;
		return true;
	}
}

void House::Draw()
{
	DrawRoof();
	DrawBase();
}

void House::DrawRoof()
{
	char space = ' ';
	int triangle_length = size + 2;         //roof overhangs base size by 2

        for (int i = 1; i <= triangle_length; i++)
        {
                for (int j = triangle_length; j >= i + 1; j--)
                {
                        cout << space;
                }

                for (int k = 1; k <= i; k++)
                {
                        if ((k == 1 || k == i) && i < triangle_length)
                                cout << border << space;
                       
                        else if ((k <= 2 || k >= i - 1) && i == triangle_length)
                                cout << border << space;
                       
                        else
                                cout << fill << space;

                }
        cout << endl;
        }
}

void House::DrawBase()
{
	char space = ' ';
	for (int i = 1; i < size; i++)
	{	
		cout << space << space;
		
		for (int j = 1; j <= size; j++)
		{
			if ((i < size) && (j == 1 || j == size))
				cout << border << space;
			else if (i == size - 1)
				cout << border << space;			
			else
				cout << fill << space;
		}
	cout << endl;	
	}
}

void House::Summary()
{
	cout << "House width and height base to roof = "  << size << " units.";
	cout << "\nPerimeter of house = " << Perimeter() << " units.";

	cout << fixed << showpoint;
	cout << setprecision(2);
	cout << "\nArea of house = " << Area() << " units.";
	cout << "\nHouse looks like:\n";
	Draw();
}


int House::GetSize() 
{ 
	return size; 
}

int House::Perimeter()
{
	return ((size + 2) * 2) + (size * 3) + 2;
}

double House::Area()
{	
	return (size*size) + (((size + 2) * (size + 2) * sqrt(3)) / 4);
}
