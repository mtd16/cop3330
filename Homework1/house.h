// All work performed by Michael DeMaria
// Section 4

class House {

public:
	
	House(int s, char b = 'X', char f = '*');

	void Grow();				// increase base size by 1
	void Shrink();				// decrease base size by 1

	bool SetBorder(char b);		// set new border character
	bool SetFill(char f);		// set a new fill character

	void Draw();				// display drawing
	
	void DrawRoof();
	void DrawBase(); 


	void Summary();				// display all info about house
								// (size, perimeter, area, and picture)

	int GetSize();				// return base size
	int Perimeter();			// return perimeter
	double Area();				// return area

private:
	int size;
	char border;
	char fill;
};
