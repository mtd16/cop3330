//Michael DeMaria
//Section 4
#include "students.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>

using namespace std;


int main()
{
	//math data
	int quiz1, quiz2, quiz3, quiz4, quiz5;
	double qzav;
	int t1, t2, m_final;

	//english data
	int paper, e_midterm, e_final;

	//history data
	int attend, proj, h_midterm, h_final;

	int numRecs;
	string first;
	string last;
	string crse;
	char in_filename[20];
	char out_filename[20];
	ifstream in;
	ofstream out;

	int numA = 0, numB = 0, numC = 0, numD = 0, numF = 0;
	char letterGrade;

	do
	{
		in.clear();

		cout << "Enter input file name: ";
		cin.getline(in_filename, 20);
		
		in.open(in_filename);
		if (!in)
			cout << "Invalid input filename. Please try again." << endl;
	
	} while (!in);

	do
	{
		out.clear();

		cout << "Enter output file name: ";
		cin.getline(out_filename, 20);

		out.open(out_filename);
		if (!out)
			cout << "Invalid output filename. Please try again." << endl;
	
	} while (!out);

	in >> numRecs;		//read in number of records in file
	in.get();

	Student* *list = new Student*[numRecs];

	for (int i = 0; i < numRecs; i++)
	{
		first = "";
		getline(in, last, ',');
		in.get();
		getline(in, first);
		in >> crse;

		if (crse == "Math")
		{
			in >> quiz1 >> quiz2 >> quiz3 >> quiz4 >> quiz5 
			   >> t1 >> t2 >> m_final;
			in.get();
			list[i] = new Math(first, last, crse, qzav, quiz1, quiz2, quiz3, quiz4, quiz5, t1, t2, m_final);
		}

		else if (crse == "English")
		{
			in >> paper >> e_midterm >> e_final;
			in.get();
			list[i] = new English(first, last, crse, paper, e_midterm, e_final);
		}

		else if (crse == "History")
		{
			in >> attend >> proj >> h_midterm >> h_final;
			in.get();
			list[i] = new History(first, last, crse, attend, proj, h_midterm, h_final);
		}
	}

	in.close();

	out << fixed << setprecision(2);

	out << "Student Grade Summary\n---------------------";
	


	out << "\n\nENGLISH CLASS \n\n";
	out << "Student\t\t\t\t\t";
	out << "Final\tFinal\tLetter\n";
	out << "Name\t\t\t\t\tExam\tAvg\tGrade";
	out << "\n----------------------------------------------------------------\n";

  	

	for (int i = 0; i < numRecs; i++)
	{
		if (list[i]->getCourse() == "English")
		{
			if (list[i]->CalculateGrade() >= 90.00)
			{	letterGrade = 'A'; numA++;	}
			else if (list[i]->CalculateGrade() >= 80.00)
			{	letterGrade = 'B'; numB++;	}
			else if (list[i]->CalculateGrade() >= 70.00)
			{	letterGrade = 'C'; numC++;	}
			else if (list[i]->CalculateGrade() >= 60.00)
			{	letterGrade = 'D'; numD++;	}
			else
			{	letterGrade = 'F'; numF++;	}


			out << list[i]->getFirstName() << " " << list[i]->getLastName();
			out << "\t\t\t\t" << list[i]->GetFinal();
			out << '\t' << list[i]->CalculateGrade();
			out << '\t' << letterGrade << "\n";
		}
	}

	out << "\n\nHISTORY CLASS \n\n";
	out << "Student\t\t\t\t\t";
	out << "Final\tFinal\tLetter\n";
	out << "Name\t\t\t\t\tExam\tAvg\tGrade";
	out << "\n----------------------------------------------------------------\n";

	for (int i = 0; i < numRecs; i++)
	{
		if (list[i]->getCourse() == "History")
		{
			if (list[i]->CalculateGrade() >= 90.00)
			{	letterGrade = 'A'; numA++;	}
			else if (list[i]->CalculateGrade() >= 80.00)
			{	letterGrade = 'B'; numB++;	}
			else if (list[i]->CalculateGrade() >= 70.00)
			{	letterGrade = 'C'; numC++;	}
			else if (list[i]->CalculateGrade() >= 60.00)
			{	letterGrade = 'D'; numD++;	}
			else
			{	letterGrade = 'F'; numF++;	}


			out << list[i]->getFirstName() << " " << list[i]->getLastName();
			out << "\t\t\t\t" << list[i]->GetFinal();
			out << '\t' << list[i]->CalculateGrade();
			out << '\t' << letterGrade << "\n";
		}
	}

	out << "\n\nMATH CLASS \n\n";
	out << "Student\t\t\t\t\t";
	out << "Final\tFinal\tLetter\n";
	out << "Name\t\t\t\t\tExam\tAvg\tGrade";
	out << "\n----------------------------------------------------------------\n";

	for (int i = 0; i < numRecs; i++)
	{
		if (list[i]->getCourse() == "Math")
		{
			if (list[i]->CalculateGrade() >= 90.00)
			{	letterGrade = 'A'; numA++;	}
			else if (list[i]->CalculateGrade() >= 80.00)
			{	letterGrade = 'B'; numB++;	}
			else if (list[i]->CalculateGrade() >= 70.00)
			{	letterGrade = 'C'; numC++;	}
			else if (list[i]->CalculateGrade() >= 60.00)
			{	letterGrade = 'D'; numD++;	}
			else
			{	letterGrade = 'F'; numF++;	}


			out << list[i]->getFirstName() << " " << list[i]->getLastName();
			out << "\t\t\t\t" << list[i]->GetFinal();
			out << '\t' << list[i]->CalculateGrade();
			out << '\t' << letterGrade << "\n";
		}
	}
		
	out << "\n\n\nOVERALL GRADE DISTRIBUTION\n\n";

	out << "A:\t" << numA << endl;
	out << "B:\t" << numB << endl;
	out << "C:\t" << numC << endl;				
	out << "D:\t" << numD << endl;
	out << "F:\t" << numF << endl;
	
	out.close();

	delete [] list;
	cout << "Processing complete.\n";

	return 0;
}