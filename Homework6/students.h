//Michael DeMaria
//Section 4
#include <iostream>
#include <string>
using namespace std;

class Student 
{
	public:
		Student(string fn, string ln, string crs);

		virtual double CalculateGrade() = 0;
		virtual int GetFinal() = 0;

		string getFirstName() const;
		string getLastName() const;
		string getCourse() const;

	protected:
		string firstName;
		string lastName;
		string course;

};

class English : public Student
{
	public:
		English(string fn, string ln, string crs, int p, int m, int f);
		double CalculateGrade();
		int GetFinal();

	private:
		int paper;
		int midterm;
		int final;
		double average;
};

class History : public Student
{
	public:
		History(string fn, string ln, string crs, int a, int p, int m, int f);
		
		double CalculateGrade();
		int GetFinal();
	private:
		int att;
		int project;
		int midterm;
		int final;
		double average;
};

class Math : public Student
{
	public:
		Math(string fn, string ln, string crs,
			 int q1, int q2, int q3, int q4, int q5, 
			 double qa, int t1, int t2, int f);
	
		double CalculateGrade();
		int GetFinal();
		

	private:
		int qz1, qz2, qz3, qz4, qz5;
		int test1, test2;
		int final;
		double qzAvg;
		double average;
};