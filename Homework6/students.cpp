//Michael DeMaria
//Section 4
#include "students.h"

Student::Student(string fn, string ln, string crs)
{
	firstName = fn;
	lastName = ln;
	course = crs;
}


//--------------------------------------------------------------

English::English(string fn, string ln, string crs, int p, int m, int f) : Student(fn, ln, crs) 
{
	paper = p;
	midterm = m;
	final = f;
}

double English::CalculateGrade()
{
	return average = (paper * .25) + (midterm * .35) + (final * .4);
}

History::History(string fn, string ln, string crs, int a, int p, int m, int f) : Student(fn, ln, crs)
{
	att = a;
	project = p;
	midterm = m;
	final = f;
}

double History::CalculateGrade()
{
	return average = (att * .1) + (project * .3) + (midterm * .3) + (final * .3);
}

Math::Math(string fn, string ln, string crs, int q1, int q2, int q3, int q4, int q5, 
		   double qa, int t1, int t2, int f) : Student(fn, ln, crs)
{
	qz1 = q1;
	qz2 = q2;
	qz3 = q3;
	qz4 = q4;
	qz5 = q5;
	qzAvg = qa;
	test1 = t1;
	test2 = t2;
	final = f;
}

double Math::CalculateGrade()
{
	qzAvg = (qz1 + qz2 + qz3 + qz4 + qz5) / 5;
	return average = (qzAvg * .15) + (test1 * .25) + (test2 * .25) + (final * .35); 
}


//---------------------getters-----------------------------------------

int English::GetFinal()
{
	return final;
}

int Math::GetFinal() 
{
	return final;
}

int History::GetFinal()
{
	return final;
}

string Student::getFirstName() const
{
	return firstName;
}

string Student::getLastName() const
{
	return lastName;
}

string Student::getCourse() const
{
	return course;
}


