//Mike DeMaria
//Section 4

#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

class Date {

public:
	Date();
	Date(int m, int d, int y);

	void Show();
	void Input();
	void Increment(int numDays = 1);
	bool Set(int m, int d, int y);
	bool SetFormat(char f);
	int Compare(const Date& d) const;

	// Accessor Functions
	int GetMonth() const;
	int GetDay() const;
	int GetYear() const;

	// Extra Credit function
	bool isLeapYear(int y) const;
	int Julian();
	
	// Custom auxilary functions 
	string MonthName(int m) const;
	int NumDaysInMonth(int m, int y) const;
	
private:
	int month;
	int day;
	int year;
	int JulianDay;
	char format;
};