//Mike DeMaria
//Section 4
#include "date.h"

Date::Date()
{
	month = 1;
	day = 1;
	year = 2000;
	format = 'D';
}

Date::Date(int m, int d, int y)
{
	if (Set(m, d, y) == false)
	{
		month = 1;
		day = 1;
		year = 2000;
		format = 'D';
	}

	else 
	{
		month = m;
		day = d;
		year = y;
		format = 'D';
	}	
}

bool Date::Set(int m, int d, int y)
{
	if (m < 1 || m > 12)
		return false;
	if (d < 1)
		return false;
	if (y < 1)
		return false;
	
	if (m == 1 && d > 31)
		return false;
	if (m == 3 && d > 31)
		return false;
	if (m == 5 && d > 31)
		return false;
	if (m == 7 && d > 31)
		return false;
	if (m == 8 && d > 31)
		return false;
	if (m == 10 && d > 31)
		return false;
	if (m == 12 && d > 31)
		return false;


	if (m == 4 && d > 30)
		return false;
	if (m == 6 && d > 30)
		return false;	
	if (m == 9 && d > 30)
		return false;
	if (m == 11 && d > 30)
		return false;

	if (m == 2)			// handles leap years
	{
		if (isLeapYear(y) == true)
		{
			if (d > 29)
				return false;
		}
		else if (isLeapYear(y) == false)
		{
			if (d > 28)
				return false;
		}
	}

	else 
	{
		month = m;
		day = d;
		year = y;
		return true;
	}
}

bool Date::isLeapYear(int y) const
{
	if (y % 4 == 0) 
	{
		if (y % 100 == 0)
			if (y % 400 != 0)
				return false;

		return true;
	}
	
	return false;
}

void Date::Input()
{
	char slash = '/';
	
	cout << "Enter a date:";
		cin >> month >> slash >> day >> slash >> year;

	while (Set(month, day, year) == false)
	{
		cout << "Invalid date. Try again.\nEnter a date: ";
		cin >> month >> slash >> day >> slash >> year;
	} 
}

void Date::Show()
{
	if (format == 'D')
		cout << month << "/" << day << "/" << year << endl;
	
	else if (format == 'T')
	{
		cout << month << "/" << day << "/";
		
		if (year % 100 < 10)
	 		cout << "0" << year % 100 << endl;

	 	else
	 		cout << year % 100 << endl;
	 }
	
	else if (format == 'L')
		cout << MonthName(month) << " " << day << ", " << year << endl;

	else if (format == 'J')
	{
		if (year % 100 < 10)
	 		cout << "0" << year % 100 << "-";

	 	else
	 		cout << year % 100 << "-";
	 	
	 	if (Julian() < 10)
	 		cout << "00" << Julian() << endl;
	 	else if (Julian() > 10 && Julian() < 100)
	 		cout << "0" << Julian() << endl;
	 	else 
	 		cout << Julian();
	}
}

bool Date::SetFormat(char f)
{
	if (f == 'D')
	{
		format = 'D';
		return true;
	}

	else if (f == 'T')
	{
		format = 'T';
		return true;
	}

	else if (f == 'L')
	{
		format = 'L';
		return true;
	}

	else if (f == 'J')
	{
		format = 'J';
		return true;
	}

	else
	{
		return false;
	}
	
}

void Date::Increment(int numDays)
{
	day+=numDays;

	while (day > NumDaysInMonth(month, year))
	{
		
		numDays = day - NumDaysInMonth(month, year);
		day = 0;
		day += numDays;
		month++;

		while (month > 12)
		{
			month = 1;
			year++;
		}
	}
}


int Date::Compare(const Date& d) const
{
	if (d.year > year)
		return -1;

	if (d.year < year)
		return 1;

	if (d.year == year)
	{
		if (d.month > month)
			return -1;

		if (d.month < month)
			return 1;
		if (d.month == month)
		{
			if (d.day > day)
				return -1;
			if (d.day < day)
				return 1;
			if (d.day == day)
				return 0;
		}

		return 0;
	}



}

int Date::NumDaysInMonth(int m, int y) const
{
	if (m == 1)
		return 31;
	if (m == 2)
	{
		if (isLeapYear(y) == true)
			return 29;
		else
			return 28;
	}

	if (m == 3)
		return 31;
	if (m == 4)
		return 30;
	if (m == 5)
		return 31;
	if (m == 6)
		return 30;
	if (m == 7)
		return 31;
	if (m == 8)
		return 31;
	if (m == 9)
		return 30;
	if (m == 10)
		return 31;
	if (m == 11)
		return 30;
	if (m == 12)
		return 31;
}

string Date::MonthName(int m) const
{
	if (m == 1)
		return "Jan";
	if (m == 2)
		return "Feb";
	if (m == 3)
		return "Mar";
	if (m == 4)
		return "Apr";
	if (m == 5)
		return "May";
	if (m == 6)
		return "June";
	if (m == 7)
		return "July";
	if (m == 8)
		return "Aug";
	if (m == 9)
		return "Sept";
	if (m == 10)
		return "Oct";
	if (m == 11)
		return "Nov";
	if (m == 12)
		return "Dec";
}

int Date::Julian()
{
	int i = 1;
	JulianDay = 0;

	while (i < month)
	{
		JulianDay += NumDaysInMonth(i, year);
		i++;
	}
	
	return JulianDay += day;
}

int Date::GetMonth() const { return month; }

int Date::GetDay() const { return day; }

int Date::GetYear() const { return year; }