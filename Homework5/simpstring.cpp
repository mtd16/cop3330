#ifndef SIMPSTRING_H
#include "simpstring.h"
#endif

#include <string.h>
#include <iostream.h>


MyString::MyString(char *x )
{
   if(x)
   {
      _length = strlen(x);
      _data = new char[_length+1];
      strcpy(_data,x);
   }
   else
   {
      _data = NULL;
      _length = 0;
   }
}
MyString::MyString(const MyString &x)
{
   if(x._length >0)
   {
      _length = x._length;
      _data = new char[_length+1];
      strcpy(_data,x._data);
   }
   else
   {
      _length=0;
      _data = NULL;
   }
}
MyString::~MyString()
{
   if(_data)
      delete [] _data;
}

MyString& MyString::operator =(const MyString &x)
{
   if(x._length > 0)
   {
      _length = x._length;
      char *temp = new char[_length+1];
      strcat(temp,x._data);
      if(_data)
         delete [] _data;
      _data = temp;
   }
   else
   {
      _length=0;
      if(_data)
         delete [] _data;
      _data = NULL;
   }
   return *this;
}
int MyString::operator ==(const MyString &s)
{
    return  !strcmp(_data,s._data);
}
int MyString::operator !=(const MyString &s)
{
    return strcmp(_data,s._data);
}
MyString MyString::operator +=(const MyString &s)
{
   _length += s._length;

   char *temp = _data;
   _data = new char[_length +1];
   strcpy(_data,temp);
   strcat(_data,s._data);
   delete [] temp;
   return *this;
}
char* MyString::c_str()const
{
   return _data;
}
int MyString::length()
{
   return _length;
}
MyString MyString::readLine(istream &in)
{
   long int eoln;
	char inval[255];
	char *ptr;
	do
	{
		in.getline(inval,255);
		ptr = strchr(inval,'\0');
		eoln = ptr -inval;
	} while (!eoln && !in.eof());
	if(_data)
      delete [] _data;
   _length = strlen(inval);
   _data = new char[_length + 1];
   strcpy(_data,inval);
   return *this;
}
istream& MyString::read(istream &in)
{
	char inval[255];
   inval[0]='\0';
   char c;
   in >> noskipws;

   do{
      in>>c;
      if(c != ' ')
      {
         char temp[2];
         temp[0] = c;
         temp[1]='\0';
         strcat(inval,temp);
      }
   }while(c!=' ');
   _length = strlen(inval);
   _data = new char[_length + 1];
   strcpy(_data,inval);
   return in;

}
ostream& MyString::write(ostream &out)const
{
  out<<_data;
  return out;
}

MyString operator + (const MyString &s1,const MyString &s2)
{
   MyString c = s1;
   c += s2;
   return c;
}

ostream& operator <<(ostream &out,const MyString &s)
{
   return s.write(out);
}

istream& operator >> (istream &in, MyString &s)
{
   return s.read(in);
}

