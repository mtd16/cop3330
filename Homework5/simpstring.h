#ifndef SIMPSTRING_H
#define SIMPSTRING_H
#include "iostream.h"
class MyString
{
public:
   MyString(char *x =0);
   MyString(const MyString &);
   ~MyString();
   MyString& operator =(const MyString&);
   int operator ==(const MyString&);
   int operator !=(const MyString&);
   MyString operator +=(const MyString&);
   char* c_str()const;
   int length();
   MyString readLine(istream&);//read to end of line
   istream& read(istream&); //read to white space
   ostream& write(ostream &)const;
private:
   char *_data;
   int _length;
};

MyString operator + (const MyString&,const MyString&);

ostream& operator <<(ostream &out,const MyString &s);

istream& operator >> (istream&, MyString&);
#endif