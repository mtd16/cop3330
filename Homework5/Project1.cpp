
#pragma hdrstop
#include <condefs.h>

#include "simpstring.h"
#include <iostream.h>
//---------------------------------------------------------------------------
USEUNIT("simpstring.cpp");
//---------------------------------------------------------------------------

void main()
{
  MyString s = "This is ";
  MyString t = "it!";
  //s.write(cout);
  MyString w;
  cout<<"Enter a string >";
  cin>>w;
  cout<<w<<endl;
  cout<<"s " <<s<<" t "<<t<<" s+t "<<(s+t)<<endl;
  if(s==t)
   cout<<"equals doesn't work";
  else
   cout<<"equals works";
  cout<<endl;
  if(s!=t)
   cout<<"not equals works";
  else
   cout<<"not equals fails";

  cout<<endl;
  char buff[255];
  strcpy(buff,s.c_str());
  cout<<"buff + t "<<buff + t<<endl;
  cin.getline(buff,254);

  int c;
  cout<<"Type an integer >";
  cin>>c;
}
 