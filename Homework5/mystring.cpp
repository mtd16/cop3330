#include "mystring.h"
#include <iomanip>
#include <cstring>
#include <cctype>

MyString::MyString()	//intializes an empty c-string
{

	size = 0;
	str = new char[1];
	str[0] = '\0';
}

MyString::MyString(const char *c)	//copies c-string argument into string object
{	

	size = strlen(c);		//get length of incoming c string
	str = new char[size + 1];
	strcpy(str, c);
}

MyString::MyString(int i)			//converts type int to char*
{

	//first get number of digits of int parameter
	int count = 0;
	if (i == 0)
		count = 1;
	else
	{
		while (i)
		{
			i /= 10;
			count++;
		}
	}

	size = count;				//num digits = size of c-string
	str = new char[size + 1];

}

MyString::MyString(const MyString& m)	//copy constructor
{
	size = m.size;
	str = new char[size + 1];
	strcpy(str, m.str);
}

MyString::~MyString()	//deallocate memory
{
	if (str != 0)
		delete [] str;
}

MyString& MyString::operator= (const MyString &m)	//overload assignment operator
{
	if (this != &m)
	{
		if (str != 0)
			delete [] str;

		size = m.size;
		str = new char[size + 1];
		strcpy(str, m.str);
	}

	return *this;
}

void MyString::Resize(int newsize)
{
	size += newsize;
	char *tempStr = new char[size + 1];

	for (int i = 0; i < size; i++)
		tempStr[i] = str[i];

	delete [] str;
	str = tempStr;
}

MyString& MyString::operator+=(const MyString& m)  // concatenation/assignment
{
	this->size += m.size;
	Resize(this->size);
	strcat(this->str, m.str);

	return *this;

}

char& MyString::operator[] (unsigned int index)
{
	if (index > size)
		cout << "Index out of bounds.";
	else
		return str[index];
}

const char& MyString::operator[] (unsigned int index) const
{
	if (index > size)
		cout << "Index out of bounds.";
	else
		return str[index];
}

// insert s into the string at position "index"
MyString& MyString::insert(unsigned int index, const MyString& s)
{


}

// find index of the first occurrence of s inside the string
//  return the index, or -1 if not found
int MyString::indexOf(const MyString& s) const
{
	


}






//operator overloads
ostream& operator<< (ostream& os, const MyString& m)	//overload output function
{
	return os << m.str;
}
 
istream& operator>> (istream& is, MyString& m)
{
	delete [] m.str;					//clear contents of m
	char *temp = new char[100];			//allocate space for user entered char
	
	is >> temp;							//read
	while (is.peek() == ' ')			// while white space, ignore
		is.ignore();
	
	m.size = strlen(temp);				//object size equal to temp's size
	m.str = new char[m.size + 1];
	strcpy(m.str, temp);				//copy temp string into MyString object
	m.str[m.size] = '\0';				//last element is null char
	delete [] temp;				

	return is;
}

istream& getline (istream& is, MyString& m, char delim)
{
	MyString temp;
	char ch;
	is.get(ch);
	while (ch != delim)
	{
		temp += ch;
		is.get(ch);
	}

	m = temp;

	return is;
}

MyString operator+ (const MyString& m1, const MyString& m2)
{
	MyString s;

	s.size = m1.size + m2.size;
	s.str = new char[s.size + 1];
	strcat(s.str, m1.str);
	strcat(s.str, m2.str);

	return s;
}

 
bool operator< (const MyString& m1, const MyString& m2)
{
	if (strcmp(m1.str, m2.str) < 0)
		return true;
	return false;
}
 
bool operator> (const MyString& m1, const MyString& m2)
{
	if (strcmp(m1.str, m2.str) > 0)
		return true;
	return false;
}
 
bool operator<=(const MyString& m1, const MyString& m2)
{
	if (strcmp(m1.str, m2.str) <= 0)
		return true;
	return false;
}
 
bool operator>=(const MyString& m1, const MyString& m2)
{
	if (strcmp(m1.str, m2.str) >= 0)
		return true;
	return false;
}
 
bool operator==(const MyString& m1, const MyString& m2 )
{
	if (strcmp(m1.str, m2.str) == 0)
		return true;
	return false;
}
 
bool operator!=(const MyString& m1, const MyString& m2)
{
	if (strcmp(m1.str, m2.str) == 0)
		return false;
	return true;
}

const char* MyString::getCString() const
{
	return str;
}

int MyString::getLength() const
{
	return size;
}