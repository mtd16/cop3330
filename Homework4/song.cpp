//Michael DeMaria Section 4
#include "song.h"
#include <iomanip>
#include <cstring>

ostream& operator<< (ostream& os, const Song& s)
{
	int i;
	os << s.title;
	for (i = strlen(s.title) + 1; i < 36; i++)
		os.put(' ');

	os << s.artist;
	for (i = strlen(s.artist) + 1; i < 21; i++)
		os.put(' ');

	if (s.category == POP)
		os << "Pop";
	if (s.category == ROCK)
		os << "Rock";
	if (s.category == ALTERNATIVE)
		os << "Alt";
	if (s.category == COUNTRY)
		os << "Ctry";
	if (s.category == HIPHOP)
		os << "HH";
	if (s.category == PARODY)
		os << "Par";

	os << "\t\t";
	os << fixed << showpoint << setprecision(2);
	os << s.size / 1000 << endl;

	return os;
}

Song::Song()
{
	strcpy(title, " ");
	strcpy(artist, " ");
	category = POP;
	size = 0;

}

void Song::Set(const char* t, const char* a, Style st, int sz)
{
	strcpy(title, t);
	strcpy(artist, a);
	category = st;
	size = sz;
}

 const char* Song::GetTitle() const
 {
 	return title;
 }

 const char* Song::GetArtist() const
 {
 	return artist;
 }
 
 int Song::GetSize() const
 {
 	return size;
 }

 Style Song::GetCategory() const
 {
 	return category;
 }