//Michael DeMaria Section 4
#include "playlist.h"
#include <iomanip>
#include <cstring>

Playlist::Playlist()
{
	maxSize = 5;
	currentSize = 0;
	songList = new Song[maxSize];
}

Playlist::~Playlist()
{
	delete [] songList;
}

void Playlist::AddSong(const char* t, const char* a, Style st, int sz)
{
	if (currentSize == maxSize)
		Grow();
	
	songList[currentSize++].Set(t, a, st, sz);
}

void Playlist::FindSong(const char* sName) const 
{
	int index = SearchSong(sName);
	if (index == -1)
		cout << "Song not found in playlist.";
	else
		cout << songList[index];
}

int Playlist::SearchSong(const char* sName) const
{
	for (int i = 0; i < currentSize; i++)
		if (strcmp(songList[i].GetTitle(), sName) == 0)
			return i;

	return -1;
}

void Playlist::FindArtist(const char* aName) const
{
	for (int i = 0; i < currentSize; i++)
		if (strcmp(songList[i].GetArtist(), aName) == 0)
			cout << songList[i];
}

void Playlist::DeleteSong(const char* sName)
{
	int index = SearchSong(sName);

	if (index == -1)
		cout << "Song not found.";
	
	else 
	{
		for (int i = index + 1; i < currentSize; i++)
			songList[i - 1] = songList[i];

		currentSize--;
		cout << "The song '" << sName << "' " << " has been removed.";
	}
}

void Playlist::ShowPlaylist()
{
	if (currentSize == 0)
	{
		cout << "\nPlaylist is empty.\n";
		return;
	}
	
	cout << "*Title*                            *Artist*            *Style*   *Size (MB)*\n\n";

	int quantity = 0;
	int totalSize = 0;
	for (int i = 0; i < currentSize; i++)
	{
		cout << songList[i];
		quantity++;
		totalSize += songList[i].GetSize();
	}
	cout << "\nNumber of songs = " << quantity;
	cout << "\nTotal playlist size = " << totalSize / 1000 << " MB\n";
}

void Playlist::CalculateSize()
{
	int totalSize = 0;
	for (int i = 0; i < currentSize; i++)
		totalSize += songList[i].GetSize();


	cout << totalSize << " kilobytes";
}

void Playlist::Grow()
{
	maxSize = currentSize + 5;
	Song* newList = new Song[maxSize];

	for (int i = 0; i < currentSize; i++)
		newList[i] = songList[i];

	delete [] songList;
	songList = newList;
}

void Playlist::CatSumm(Style st)
{
	int quantity = 0;
	int size = 0;
	
	cout << "\n*Title*                            *Artist*            *Style*   *Size (MB)*\n\n";

	for (int i = 0; i < currentSize; i++)
		if (songList[i].GetCategory() == st)
		{
			cout << songList[i];
			quantity++;
			size+=songList[i].GetSize();
		}
	cout << "\n\nNumber of songs that match this category: " << quantity;
	cout << fixed << showpoint << setprecision(2);			//Set precision not working here?
	cout << "\nTotal size: " << size / 1000 << " MB";
			
}