//Michael DeMaria Section 4
#include <iostream>
#include "playlist.h"
#include <cstring>
#include <iomanip>

using namespace std;

void ShowMenu();
char Legal(char c);
char StyleLegal(char c);
char GetCommand();
char GetChar(const char* promptString);

int main(){

	Playlist p;		//empty playlist object
	char t[36];		//song title;
	char a[21];		//artist name
	char cat;		//user category selection
	Style st;		//style enum
	int sz;			//user size
	char choice;	

	ShowMenu();

	char command;

	do {
		command = GetCommand();
		switch (command)
		{
			case 'A':
			{
				cout << "\nEnter a song title: ";
					cin.getline(t, 36);
				cout << "Enter an artist: ";
					cin.getline(a, 21);
				cout << "Enter a category: (P)op, (R)ock, (A)lternative, (C)ountry, (H)iphop, or Parod(Y): ";
					cin >> cat;
				cat = toupper(cat);
				while (!StyleLegal(cat))
				{
					cout << "Invalid category entry! Please try again.";
					cout << "\nEnter a category: (P)op, (R)ock, (A)lternative, (C)ountry, (H)iphop, or Parod(Y): ";
					cin >> cat;
					cat = toupper(cat);
				} 

				if (cat == 'P')
					st = POP;
				else if (cat == 'R')
					st = ROCK;
				else if (cat == 'A')
					st = ALTERNATIVE;
				else if (cat == 'C')
					st = COUNTRY;
				else if (cat == 'H')
					st = HIPHOP;
				else if (cat == 'Y')
					st = PARODY;

				cout << "Enter a size: ";
					cin >> sz;
				while (sz < 0)
				{
					cout << "Error: size must be a positive integer. Try again.\n";
					cout << "Enter a size: ";
						cin >> sz;
				}

				p.AddSong(t, a, st, sz);
				break;
			}

			case 'F':
			{
				cout << "Search by (S)ong or (A)rtist? ";
					cin >> choice;
				choice = toupper(choice);
				while (choice != 'S' && choice != 'A')
				{
					cout << "Invalid selection. Try again.\n";
					cout << "Search by (S)ong or (A)rtist?";
						cin >> choice;
					choice = toupper(choice);
				}
				switch (choice)
				{
					case 'S':
					{
						cout << "Enter a song title: ";
						cin.ignore();
						cin.getline(t, 36);		//user enters song title
						p.FindSong(t);
						break;
					}
					case 'A':
					{
						cout << "Enter an artist: ";
						cin.ignore();
						cin.getline(a, 21);
						p.FindArtist(a);
						break;
					}

				}
				break;
			}

			case 'D':
			{
				cout << "Enter the name of the song you wish to delete: ";
				cin.getline(t, 36);
				p.DeleteSong(t);
				break;
			}

			case 'S': p.ShowPlaylist(); break;
			case 'C':
			{
				cout << "Enter a category: (P)op, (R)ock, (A)lternative, (C)ountry, (H)iphop, or Parod(Y): ";
					cin >> cat;
					cat = toupper(cat);
				while (!StyleLegal(cat))
				{
					cout << "Invalid category entry! Please try again.";
					cout << "\nEnter a category: (P)op, (R)ock, (A)lternative, (C)ountry, (H)iphop, or Parod(Y): ";
					cin >> cat;
					cat = toupper(cat);
				} 

				if (cat == 'P')
					st = POP;
				else if (cat == 'R')
					st = ROCK;
				else if (cat == 'A')
					st = ALTERNATIVE;
				else if (cat == 'C')
					st = COUNTRY;
				else if (cat == 'H')
					st = HIPHOP;
				else if (cat == 'Y')
					st = PARODY;
				p.CatSumm(st);
				break;
			}

			case 'Z': p.CalculateSize(); break;
			case 'M': ShowMenu(); break;
		}


	} while (command != 'X');
	
	cout << "\nAdios!\n\n";
	return 0;

}

void ShowMenu()
{
	cout << "*** Welcome to Playlist Manager ***\n\n";
	cout << "\t\t*** PLAYLIST MENU***\n";
	cout << "\tA:\tAdd a song to the playlist\n";
	cout << "\tF:\tFind a song on the playlist\n";
	cout << "\tD:\tDelete a song from the playlist\n";
	cout << "\tS:\tShow the entire playlist\n";
 	cout << "\tC:\tCategory summary\n";
 	cout << "\tZ:\tShow playlist size\n";
 	cout << "\tM:\tShow this Menu\n";
  	cout << "\tX:\teXit the program\t";
}

char GetChar(const char* promptString)
{
	char response;						//char to be returned

	cout << promptString;				//prompt user
	cin >> response;					//read in char
	response = toupper(response);		//convert to uppercase
	cin.get();							//discards newline
	return response;
}

char Legal(char c)		//represents legal character options for user to enter
{
	return ((c == 'A') || (c == 'F') || (c == 'D') || 
			(c == 'S') || (c == 'C') || (c == 'Z') || 
			(c == 'Z') || (c == 'M') || (c == 'X'));
}

char StyleLegal(char c)
{
	return ((c == 'P') || (c == 'R') || (c == 'A') ||
			(c == 'C') || (c == 'H') || (c == 'Y'));
}

char GetCommand()
{
	char cmd = GetChar("\n\n>");		//get command char

	while (!Legal(cmd))					//loop menu until user enters legal command
	{
		cout << "\nInvalid option, please try again\n";
		ShowMenu();
		cmd = GetChar("\n\n>");
	}
	return cmd;
}
