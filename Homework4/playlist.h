//Michael DeMaria Section 4
#include <iostream>
#include "song.h"
using namespace std;

class Playlist {

	public:
		Playlist();					//create empty playlist
		~Playlist();				//deallocate playlist
		
		void AddSong(const char* t, const char* a, Style st, int sz);				//Add a song to the playlist
		void FindSong(const char* sName) const;										//Find a song in the playlist
		void FindArtist(const char* aName) const;									//Search for artist in playlist
		void DeleteSong(const char* sName);											//Delete a song from the playlist
		void ShowPlaylist();														//Show the entire playlist
		void CatSumm(Style st);														//Category summary
		void CalculateSize();														//Show total playlist size (in kb)
		
	private:
		int maxSize;
		int currentSize;
		Song* songList;
		void Grow();
		int SearchSong(const char* sName) const;
		
		

};