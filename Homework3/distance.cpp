//Michael DeMaria
//Section 4
//Assignment 3

#include "distance.h"

Distance::Distance()
{
	miles = 0;
	yards = 0;
	feet = 0;
	inches = 0;
}

Distance::Distance(int i)
{

	if (i < 0)
	{
		miles = 0;
		yards = 0;
		feet = 0;
		inches = 0;
	}
	else 
	{
		miles = 0;
		yards = 0;
		feet = 0;
		inches = i;

		Simplify();
	}
	
}

Distance::Distance(int m, int y, int f, int i)
{
	if (m < 0 || y < 0 || f < 0 || i < 0)
	{	
		miles = 0;
		yards = 0;
		feet = 0;
		inches = 0;
	}

	else
	{
		Simplify();
		miles = m;
		yards = y;
		feet = f;
		inches = i; 
	}
}
ostream& operator<< (ostream& os, const Distance& d)
{

	os << "(";

	if (d.miles != 0)
		os << d.miles << "m ";
	if (d.yards != 0)
		os << d.yards << "y ";
	if (d.feet != 0)
		os << d.feet << "' ";
	
	os << d.inches << '"' << ')' << endl;

	
	return os;
}

istream& operator>> (istream& is, Distance& d)
{
	char comma;
	is >> d.miles >> comma
	   >> d.yards >> comma
	   >> d.feet >> comma
	   >> d.inches;

	if (d.miles < 0 || d.yards < 0 || d.feet < 0 || d.inches < 0)
	{	
		d.miles = 0;
		d.yards = 0;
		d.feet = 0;
		d.inches = 0;
	}

	d.Simplify();

	return is;
}
Distance operator+ (const Distance& d1, const Distance& d2)
{
	Distance sum;

	sum.miles = d1.miles + d2.miles;
	sum.yards = d1.yards + d2.yards;
	sum.feet = d1.feet + d2.feet;
	sum.inches = d1.inches + d2.inches;

	sum.Simplify();
	return sum;
}

Distance operator- (const Distance& d1, const Distance& d2)
{
	Distance diff;		//difference

	diff.miles = d1.miles - d2.miles;
	diff.yards = d1.yards - d2.yards;
	diff.feet = d1.feet - d2.feet;
	diff.inches = d1.inches - d2.inches;

	if (diff.miles < 0)
		diff.miles = 0;
	if (diff.yards < 0)
		diff.yards = 0;
	if (diff.feet < 0)
		diff.feet = 0;
	if (diff.inches < 0)
		diff.inches = 0;

	diff.Simplify();
	return diff;
}

Distance operator* (const Distance& d, int m)
{
	Distance p;			//product

	p.miles = d.miles * m;
	p.yards = d.yards * m;
	p.feet = d.feet * m;
	p.inches = d.inches * m;

	p.Simplify();
	return p;
}



bool operator< (const Distance& d1, const Distance& d2)
{
	if (d1.miles < d2.miles)
		return true;
	else if (d1.yards < d2.yards)
		return true;
	else if (d1.feet < d2.feet)
		return true;
	else if (d1.inches < d2.inches)
		return true;
	else
		return false;
}

bool operator> (const Distance& d1, const Distance& d2)
{
	if (d1.miles > d2.miles)
		return true;
	else if (d1.yards > d2.yards)
		return true;
	else if (d1.feet > d2.feet)
		return true;
	else if (d1.inches > d2.inches)
		return true;
	else
		return false;
}

bool operator<= (const Distance& d1, const Distance& d2)
{
	if (d1.miles <= d2.miles)
		return true;
	else if (d1.yards <= d2.yards)
		return true;
	else if (d1.feet <= d2.feet)
		return true;
	else if (d1.inches <= d2.inches)
		return true;
	else
		return false;
}

bool operator>= (const Distance& d1, const Distance& d2)
{
	if (d1.miles >= d2.miles)
		return true;
	else if (d1.yards >= d2.yards)
		return true;
	else if (d1.feet >= d2.feet)
		return true;
	else if (d1.inches >= d2.inches)
		return true;
 	else
		return false;
}

bool operator== (const Distance& d1, const Distance& d2)
{
	if ((d1.miles == d2.miles) &&
		(d1.yards == d2.yards) &&
		(d1.feet == d2.feet)   &&
		(d1.inches == d2.inches))
			return true;

	else
		return false;
}
bool operator!= (const Distance& d1, const Distance& d2)
{
	if ((d1.miles != d2.miles) &&
		(d1.yards != d2.yards) &&
		(d1.feet != d2.feet)   &&
		(d1.inches != d2.inches))
			return true;
	else
		return false;
}

Distance& Distance::operator++()
{
	inches = inches + 1;
	if (inches >= 12)
		Simplify();

	return *this;
}

Distance Distance::operator++(int)
{
	Distance temp = *this;
	inches = inches + 1;
	if (inches >= 12)
		Simplify();

	return temp;
}

Distance& Distance::operator--()
{
	inches = inches - 1;
	if (inches < 0)
		inches = 0;
	return *this;
}

Distance Distance::operator--(int)
{
	Distance temp = *this;
	inches = inches - 1;
	if (inches < 0)
		inches = 0;
	return temp;
}






void Distance::Simplify()
{
	while (inches >= 12)
	{
		int temp = inches;
		inches = temp - 12;
		feet++;
	}

	while (feet >= 3)
	{
		int temp = feet;
		feet = temp - 3;			//3 feet to 1 yard
		yards++;
	}

	while (yards >= 1760 )
	{
		int temp = yards;
		yards = temp - 1760;		//1760 yards to 1 mile
		miles++;
	}
}

