//Michael DeMaria
//Section 4
//Assignment 3

#include <iostream>
using namespace std;

class Distance {

friend ostream& operator<< (ostream& os, const Distance& d);
friend istream& operator>> (istream& is, Distance& d);
friend Distance operator+ (const Distance& d1, const Distance& d2);
friend Distance operator- (const Distance& d1, const Distance& d2);
friend Distance operator* (const Distance& d, int m);

friend bool operator< (const Distance& d1, const Distance& d2);
friend bool operator> (const Distance& d1, const Distance& d2);
friend bool operator<= (const Distance& d1, const Distance& d2);
friend bool operator>= (const Distance& d1, const Distance& d2);
friend bool operator== (const Distance& d1, const Distance& d2);
friend bool operator!= (const Distance& d1, const Distance& d2);



public:
	Distance();
	Distance(int);
	Distance(int m, int y, int f, int i);
	
	Distance& operator++();
	Distance operator++(int);
	Distance& operator--();
	Distance operator--(int);

private:
	int miles, yards, feet, inches;
	void Simplify();

};